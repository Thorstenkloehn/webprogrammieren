package verwalten

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"github.com/russross/blackfriday"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
	"text/template"
)

type Webseiten struct {
	WebsiteTitel    string
	Titel           string
	Sidebar         []string
	Seiten          string
	Inhalt          string
	Hosting         string
	SidebarVorschau string
	Zugriff         bool
}

var vorlagen, _ = template.ParseGlob("vorlagen/*")

func (d *Webseiten) Anzeigen(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	d.Seiten = vars["Seiten"]
	d.Zugriff = false
	d.Hosting = os.Args[1] + "://" + r.Host
	if d.Seiten == "" {

		d.Seiten = "index"
		if d.Titel == "" {
			d.Titel = "home"
		}
	}
	jsonDateieinlesen, _ := ioutil.ReadFile("config.json")
	json.Unmarshal(jsonDateieinlesen, &d)
	sidebardatei, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/Sidebar.txt")
	ausgabe := strings.Split(string(sidebardatei), ",")
	d.Sidebar = ausgabe
	dateiausgabe, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/" + d.Seiten + ".md")
	output := blackfriday.MarkdownCommon(dateiausgabe)
	d.Inhalt = string(output)
	vorlagen.ExecuteTemplate(w, "index.html", d)

}
func (d *Webseiten) LocalAnzeigen(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	d.Seiten = vars["Seiten"]
	d.Zugriff = true
	d.Hosting = os.Args[1] + "://" + r.Host
	if d.Seiten == "" {

		d.Seiten = "index"
		if d.Titel == "" {
			d.Titel = "home"
		}
	}
	jsonDateieinlesen, _ := ioutil.ReadFile("config.json")
	json.Unmarshal(jsonDateieinlesen, &d)
	sidebardatei, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/Sidebar.txt")
	ausgabe := strings.Split(string(sidebardatei), ",")
	d.Sidebar = ausgabe
	dateiausgabe, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/" + d.Seiten + ".md")
	output := blackfriday.MarkdownCommon(dateiausgabe)
	d.Inhalt = string(output)
	vorlagen.ExecuteTemplate(w, "index.html", d)

}
func (d *Webseiten) Vorschau(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	d.Seiten = vars["Seiten"]
	d.Hosting = os.Args[1] + "://" + r.Host

	jsonDateieinlesen, _ := ioutil.ReadFile("config.json")
	json.Unmarshal(jsonDateieinlesen, &d)
	sidebardatei, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/Sidebar.txt")
	d.SidebarVorschau = string(sidebardatei)
	dateiausgabe, _ := ioutil.ReadFile("inhalt/" + d.Titel + "/" + d.Seiten + ".md")

	d.Inhalt = string(dateiausgabe)

	d.Zugriff = true
	vorlagen.ExecuteTemplate(w, "vorschau.html", d)
	return

}

func (d *Webseiten) Bearbeiten(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	d.Titel = vars["Titel"]
	os.Mkdir("inhalt/"+d.Titel, 0777)
	d.Seiten = vars["Seiten"]
	d.Hosting = os.Args[1] + "://" + r.Host

	d.Zugriff = true
	d.SidebarVorschau = r.FormValue("Sidebar")
	d.Inhalt = r.FormValue("Inhalt")
	ioutil.WriteFile("inhalt/"+d.Titel+"/Sidebar.txt", []byte(d.SidebarVorschau), 0777)
	ioutil.WriteFile("inhalt/"+d.Titel+"/"+d.Seiten+".md", []byte(d.Inhalt), 0777)

	// vorlagen.ExecuteTemplate(w, "bearbeiten.html", d)
	http.Redirect(w, r, os.Args[1]+"://"+r.Host+"/"+d.Titel+"/"+d.Seiten, 301)

}
