package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/webprogrammieren-de/Golang/verwalten"
	"io/ioutil"
	"net/http"
	"os"
)

var start = verwalten.Webseiten{}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("Geben Sie bitte 'webprogrammieren http oder https' ein")
		os.Exit(0)

	}
	filename := "config.json"
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Println("Erstellen Sie eine neue Datei  ")
		os.Exit(0)
	}
	d := verwalten.Webseiten{}
	jsonDateieinlesen, _ := ioutil.ReadFile("config.json")
	json.Unmarshal(jsonDateieinlesen, &d)
	muxhandler := mux.NewRouter()
	muxhandler.HandleFunc("/", start.Anzeigen).Host(d.WebsiteTitel)
	muxhandler.HandleFunc("/{Titel}/{Seiten}", start.Anzeigen).Host(d.WebsiteTitel)
	muxhandler.HandleFunc("/{Titel}", start.Anzeigen).Host(d.WebsiteTitel)
	muxhandler.HandleFunc("/", start.LocalAnzeigen).Host("localhost")
	muxhandler.HandleFunc("/{Titel}/{Seiten}", start.LocalAnzeigen).Host("localhost")
	muxhandler.HandleFunc("/{Titel}", start.LocalAnzeigen).Host("localhost")
	muxhandler.HandleFunc("/vorschau/{Titel}/{Seiten}", start.Vorschau).Host("localhost")
	muxhandler.HandleFunc("/bearbeitung/{Titel}/{Seiten}", start.Bearbeiten).Host("localhost")
	muxhandler.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
	switch os.Args[1] {
	case "http":
		http.ListenAndServe(":80", handlers.CompressHandler(muxhandler))
	case "https":
		http.ListenAndServeTLS(":443", "/etc/letsencrypt/live/"+d.WebsiteTitel+"/cert.pem", "/etc/letsencrypt/live/"+d.WebsiteTitel+"/privkey.pem", handlers.CompressHandler(muxhandler))

	default:
		http.ListenAndServe(":80", handlers.CompressHandler(muxhandler))
	}
}
