## Versionsverwaltungssoftware

* [Git](https://git-scm.com/)

## Programmiersprachen

*  [Go](https://golang.org/)
* [Buildtools für Visual Studio](https://visualstudio.microsoft.com/de/downloads/)
* [Clang](http://releases.llvm.org/download.html)
* [GCC](http://mingw-w64.org/doku.php)
* [Rust](https://www.rust-lang.org/tools/install)

##  Kostenlos Editor

* [Atom](https://atom.io/) - [Awesome](https://github.com/mehcode/awesome-atom)
* [Visual Studio Code](https://code.visualstudio.com/) - [Awesome](https://github.com/viatsko/awesome-vscode)

## Integrierte Entwicklungsumgebung

* [Clion](https://www.jetbrains.com/de-de/clion/)
* [IntelliJ IDEA](https://www.jetbrains.com/idea/)
* [Goland](https://www.jetbrains.com/de-de/go/)
