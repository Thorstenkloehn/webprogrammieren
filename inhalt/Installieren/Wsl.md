## Windows-Subsystem für Linux (Ubuntu)

### PowerShell als Administrator

```

Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

```
### Windows Store
* Ubuntu 18.04 Installieren.

### Go

```

wget https://dl.google.com/go/go1.13.6.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf go1.13.6.linux-amd64.tar.gz
sudo nano ~/.bashrc
----Datei----
export GOROOT=/usr/local/go
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
---Datei ende---
source ~/.bashrc


```

## C und C++

```

sudo apt update
sudo apt-get install cmake gcc clang gdb build-essential


```

### Webassembly

```
sudo apt update
 sudo apt upgrade
 sudo apt install python2.7 python-pip
 sudo apt-get install git
 sudo mkdir  /emsdk
 cd /
 sudo chmod 777 -R  emsdk
 cd  /emsdk
 git clone https://github.com/emscripten-core/emsdk.git .

 ./emsdk install latest
 ./emsdk activate latest
 sudo nano ~/.bashrc
 _____Datei_____
 source  /emsdk/./emsdk_env.sh --build=Release

____Ende._____
Include Pfad ist /emsdk/upstream/emscripten/system/include

source ~/.bashrc


```

## Rust

```
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh


```

