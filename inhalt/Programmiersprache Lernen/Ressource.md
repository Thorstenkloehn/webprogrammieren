## Verlage
* https://www.packtpub.com/

## Websiten

* https://www.w3schools.com/
* https://hackr.io/
* https://www.sololearn.com/
* https://www.tutorialspoint.com/tutorialslibrary.htm
* https://www.javatpoint.com/
* http://www.java2s.com/
* https://github.com/sindresorhus/awesome

## Video
* https://www.udemy.com/
* https://www.lynda.com/

### C
* [C Programmieren - Praxisorientierter Einsteiger Kurs](https://www.udemy.com/course/c-programmieren-praxisorientierter-einsteiger-kurs/)
* [C Programmieren für Anfänger](https://www.udemy.com/course/c-programmieren-fur-anfanger/)
* [C Komplettkurs: Praxisnahe Programmierung für C Einsteiger](https://www.udemy.com/course/c-programmierung-praxisnaher-komplettkurs-fur-einsteiger/)

### C++

*  [C++ Bootcamp: Vom Anfänger zum C++ - Entwickler!](https://www.udemy.com/course/cpp-bootcamp/)
*  [C++ Komplettkurs: Praxisnahe und Moderne C++ Programmierung](https://www.udemy.com/course/der-komplettkurs-zur-modernen-c-programmierung/)

### Go

* [ Golang](https://www.youtube.com/watch?v=9GAYj92SqOc&list=PLStQc0GqppuXxopO-pCAJWyqP7a5_IwSg)
* [Einführung in Go](https://www.udemy.com/course/einfuhrung-in-go/)



### Rust

*  [Rust Programmieren lernen](https://www.udemy.com/course/rust-programmieren/)

## Bücher

### C Programmieren

* [Hands-On-Netzwerkprogrammierung mit C] (https://subscription.packtpub.com/book/cloud_and_networking/9781789349863)