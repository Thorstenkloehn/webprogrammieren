## Kommentare
### Go
```
package main
// Einzeilige Kommentare
func main() {

	/*

		   Mehrzeilige Kommentare
		   demo
		   demo
		   
	*/

}
```
### C
```
void main() {  // Einzeilige Kommentare

/*

 Mehrzeilige 
 
 Kommentare


*/

 }

```
### C++

```
void main() {  // Einzeilige Kommentare

/*

 Mehrzeilige 
 
 Kommentare


*/

 }

```

###  Rust

```

fn main() {

    println!("Textausgabe"); // Einzeilige Kommentare


    /*  



    */


}



```
