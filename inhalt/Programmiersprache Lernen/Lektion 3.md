## Go
* bool
* byte
* int
* float
* string

### C
* bool     #include stdbool.h
*  char
* int
*  float
* void

### C++
* bool
* char
* int
* float
* void
* std::string

###  Rust

* bool
* i32
* f64
* &str

