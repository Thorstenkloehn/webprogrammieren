## Rust
### Datei öffnen
```
use std::fs::File;
use std::io::prelude::*;
fn datei_lesen (dateiname: &str) -> String {
    let mut file = File::open(dateiname).expect("Kann nicht geöffnet werden");
    let mut inhalt = String::new();
    file.read_to_string(&mut inhalt).expect("Kann nicht geöffnet werden");
     inhalt
}
fn main() {
    
    println!("{}",datei_lesen("foot.txt"))
}

```